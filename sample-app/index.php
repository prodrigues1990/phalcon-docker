<?php

/**
 * This file is copied from the Phalcon Developer Tools.
 *
 * See README.md on this project.
 */

$uri = urldecode(parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH));

if ($uri !== '/' && file_exists(__DIR__ . '/sample-app' . $uri)) {
    return false;
}

$_GET['_url'] = $_SERVER['REQUEST_URI'];

require_once __DIR__ . '/sample-app/.htrouter.php';

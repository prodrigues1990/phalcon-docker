# Creating a Phalcon Application

Copy the `docker-compose` files in sample-app directory to your project directory. They provide a runtime and development environment for Phalcon in docker. To create a Phalcon project, or run any other Phalcon Devtools command.

```sh
export APP=sample-app   ## replace with app name
docker-compose -f docker-compose.yml -f docker-compose.dev.yml up -d
docker exec -it sampleapp_cli_1 phalcon create-project ${APP}
sudo chgrp -R $(whoami) sample-app && sudo chmod g+w ${APP}
sed -i s/sample-app/${APP}/ index.php
```

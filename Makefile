.PHONY: all runtime devtime

all: runtime devtime

runtime: runtime.Dockerfile
	docker build -t phalcon-runtime -f runtime.Dockerfile .

devtime: runtime devtime.Dockerfile
	docker build -t phalcon-devtime -f devtime.Dockerfile .

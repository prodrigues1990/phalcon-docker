FROM phalcon-runtime

ARG PHALCON_DEVTOOLS_VERSION=4.0.2

RUN curl -LO https://github.com/phalcon/phalcon-devtools/archive/v${PHALCON_DEVTOOLS_VERSION}.tar.gz && \
    tar xzf ${PWD}/v${PHALCON_DEVTOOLS_VERSION}.tar.gz && \
    ln -s ${PWD}/phalcon-devtools-${PHALCON_DEVTOOLS_VERSION}/phalcon /usr/bin/phalcon && \
    chmod +x /usr/bin/phalcon && \
    cd ${PWD}/phalcon-devtools-${PHALCON_DEVTOOLS_VERSION} && \
    composer install && \
    cd .. && \
    rm -r ${PWD}/v${PHALCON_DEVTOOLS_VERSION}.tar.gz

WORKDIR /app
